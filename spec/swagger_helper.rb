require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.to_s + '/swagger'

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:to_swagger' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
      'v1/swagger.json' => {
          swagger: '2.0',
          info: {
              title: 'Psi',
              version: 'v1'
          },
          definitions: {
              city_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "name": "Курган"
                  }
              },
              send_city_map: {
                  type: 'object',
                  properties: {
                      name: {type: :string}
                  },
                  required: ['name'],
                  example: {
                      "name": "Москва"
                  }
              },
              cities_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "Курган"
                          },
                          {
                              "id": 2,
                              "name": "Москва"
                          }
                      ]
              },
              school_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string},
                      city_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "name": "Общеобразовательная школа №1",
                      "city_id": 1
                  }
              },
              send_school_map: {
                  type: 'object',
                  properties: {
                      name: {type: :string},
                      city_id: {type: :integer}
                  },
                  required: ['name', 'city_id'],
                  example: {
                      "name": "Общеобразовательная школа №1",
                      "city_id": 1
                  }
              },
              schools_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string},
                          city_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "Общеобразовательная школа №1",
                              "city_id": 1
                          },
                          {
                              "id": 2,
                              "name": "Общеобразовательная школа №2",
                              "city_id": 2
                          }
                      ]
              },
              personal_value_group_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string},
                      human_name: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "name": "коммуникативные",
                  }
              },
              personal_value_groups_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string},
                          human_name: {type: :string}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "коммуникативные"
                          },
                          {
                              "id": 2,
                              "name": "альтруистические"
                          }
                      ]
              },
              characteristic_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string},
                      positivity: {
                          type: :number,
                          minimum: -1,
                          maximum: 1
                      },
                      personal_value_group_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "name": "Умный",
                      "positivity": 1,
                      "personal_value_group_id": 1
                  }
              },
              send_characteristic_map: {
                  type: 'object',
                  properties: {
                      name: {type: :string},
                      positivity: {
                          type: :number,
                          minimum: -1,
                          maximum: 1
                      },
                      personal_value_group_id: {type: :integer}
                  },
                  required: ['name', 'personal_value_id'],
                  example: {
                      "name": "Умный",
                      "positivity": 1,
                      "personal_value_group_id": 1
                  }
              },
              characteristics_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string},
                          positivity: {
                              type: :number,
                              minimum: -1,
                              maximum: 1
                          },
                          personal_value_group_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "Умный",
                              "positivity": 1,
                              "personal_value_group_id": 1
                          },
                          {
                              "id": 2,
                              "name": "Тупой",
                              "positivity": -1,
                              "personal_value_group_id": 2
                          }
                      ]
              },
              role_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "name": "Пекарь",
                  }
              },
              roles_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "Пекарь"
                          },
                          {
                              "id": 2,
                              "name": "Художник"
                          }
                      ]
              },
              student_group_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      school_id: {type: :integer},
                      letter: {type: :string},
                      form_date: {type: :string},
                      name: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "school_id": 1,
                      "letter": "A",
                      "form_date": "2019-09-01",
                      "name": "1A"
                  }
              },
              send_student_group_map: {
                  type: 'object',
                  properties: {
                      school_id: {type: :integer},
                      letter: {type: :string},
                      form_date: {type: :string},
                  },
                  required: ['school_id', 'letter', 'form_date'],
                  example: {
                      "school_id": 1,
                      "letter": "A",
                      "form_date": "2019-09-01"
                  }
              },
              student_groups_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          school_id: {type: :integer},
                          letter: {type: :string},
                          form_date: {type: :string},
                          name: {type: :string}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "school_id": 1,
                              "letter": "A",
                              "form_date": "2019-09-01",
                              "name": "1A"
                          },
                          {
                              "id": 2,
                              "school_id": 2,
                              "letter": "Б",
                              "form_date": "2018-09-01",
                              "name": "2A"
                          }
                      ]
              },
              student_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      student_group_id: {type: :integer},
                      name: {type: :string},
                      sex: {
                          type: :string,
                          enum: [:male, :female]
                      },
                      token: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "student_group_id": 1,
                      "name": "alex",
                      "sex": "male",
                      "token": '6Tys78As'
                  }
              },
              send_student_map: {
                  type: 'object',
                  properties: {
                      student_group_id: {type: :integer},
                      name: {type: :string},
                      sex: {
                          type: :string,
                          enum: [:male, :female]
                      }
                  },
                  required: ['student_group_id', 'name', 'sex'],
                  example: {
                      "student_group_id": 1,
                      "name": 'alex',
                      "sex": 'male'
                  }
              },
              students_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          student_group_id: {type: :integer},
                          name: {type: :string},
                          sex: {
                              type: :string,
                              enum: [:male, :female]
                          },
                          token: {type: :string}
                      }
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "student_group_id": 1,
                              "name": "alex",
                              "sex": "male",
                              "token": '6Tys78As'
                          },
                          {
                              "id": 2,
                              "student_group_id": 1,
                              "name": "lena",
                              "sex": "female",
                              "token": '8Yu2a8Ss'
                          }
                      ]
              },

              test_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      student_group_id: {type: :integer},
                      start: {type: :string},
                      finish: {type: :string}
                  },
                  example: {
                      "id": 1,
                      "student_group_id": 1,
                      "start": "2019-01-12T18:18",
                      "finish": "2019-01-13T12:10"
                  }
              },
              send_test_map: {
                  type: 'object',
                  properties: {
                      student_group_id: {type: :integer},
                      start: {type: :string},
                      finish: {type: :string}
                  },
                  required: ['student_group_id', 'start', 'finish'],
                  example: {
                      "student_group_id": 1,
                      "start": "2019-01-12T18:18",
                      "finish": "2019-01-13T12:10"
                  }
              },
              tests_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          student_group_id: {type: :integer},
                          start: {type: :string},
                          finish: {type: :string}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "student_group_id": 1,
                              "start": "2019-01-12T18:18",
                              "finish": "2019-01-13T12:10"
                          },
                          {
                              "id": 2,
                              "student_group_id": 13,
                              "start": "2018-03-12T08:18",
                              "finish": "2018-03-13T12:10"
                          }
                      ]
              },
              student_test_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      test_id: {type: :integer},
                      student_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "test_id": 1,
                      "student_id": 112
                  }
              },
              send_student_test_map: {
                  type: 'object',
                  properties: {
                      test_id: {type: :integer},
                      student_id: {type: :integer},
                      persons_attributes: {
                          type: 'array',
                          items: {
                              properties: {
                                  name: {type: :string},
                                  x: {type: :number},
                                  y: {type: :number},
                                  person_roles_attributes: {
                                      type: 'array',
                                      items: {
                                          properties: {
                                              role_id: {type: :integer}
                                          }
                                      }
                                  },
                                  person_characteristics_attributes: {
                                      type: 'array',
                                      items: {
                                          properties: {
                                              characteristic_id: {type: :integer}
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  },
                  required: ['test_id', 'student_id'],
                  example: {
                      "test_id": 1,
                      "student_id": 112,
                      "persons_attributes": [
                          {
                            "name": "Дима",
                            "x": 0.2653061224489796,
                            "y": 0.6599999999999999,
                            "person_roles_attributes": [
                                {
                                    "role_id": 1
                                },
                                {
                                    "role_id": 2
                                }
                            ],
                            "person_characteristics_attributes": [
                                {
                                    "characteristic_id": 12
                                },
                                {
                                    "characteristic_id": 13
                                }
                            ]
                          },
                          {
                              "name": "Вова",
                              "x": 0.26,
                              "y": 0.9,
                              "person_roles_attributes": [
                                  {
                                      "role_id": 133
                                  }
                              ],
                              "person_characteristics_attributes": [
                                  {
                                      "characteristic_id": 131
                                  }
                              ]
                          }
                      ]
                  }
              },
              student_tests_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          test_id: {type: :integer},
                          student_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "test_id": 1,
                              "student_id": 112
                          },
                          {
                              "id": 2,
                              "test_id": 2,
                              "student_id": 12
                          }
                      ]
              },
              person_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      name: {type: :string},
                      x: {type: :number,
                          minimum: 0,
                          maximum: 1
                      },
                      y: {type: :number,
                          minimum: 0,
                          maximum: 1
                      },
                      student_test_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "name": "Иванов И.И.",
                      "x": 0.5,
                      "y": 0.9,
                      "student_test_id": 12
                  }
              },
              send_person_map: {
                  type: 'object',
                  properties: {
                      name: {type: :string},
                      x: {type: :number,
                          minimum: 0,
                          maximum: 1
                      },
                      y: {type: :number,
                          minimum: 0,
                          maximum: 1
                      },
                      student_test_id: {type: :integer},
                      person_roles_attributes: {
                          type: 'array',
                          items:{
                              properties: {
                                  role_id: {type: 'integer'}
                              }
                          }
                      },
                      person_characteristics_attributes: {
                          type: 'array',
                          items:{
                              properties: {
                                  characteristic_id: {type: 'integer'}
                              }
                          }
                      }
                  },
                  required: ['name', 'x', 'y', 'student_test'],
                  example: {
                      "name": "Иванов И.И.",
                      "x": 0.5,
                      "y": 0.9,
                      "student_test_id": 12,
                      "person_roles_attributes": [
                          {
                              "role_id": 1
                          },
                          {
                              "role_id": 2
                          }
                      ],
                      "person_characteristics_attributes": [
                          {
                              "characteristic_id": 12
                          },
                          {
                              "characteristic_id": 13
                          }
                      ]
                  }
              },
              persons_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          name: {type: :string},
                          x: {type: :number,
                              minimum: 0,
                              maximum: 1
                          },
                          y: {type: :number,
                              minimum: 0,
                              maximum: 1
                          },
                          student_test_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "name": "Иванов И.И.",
                              "x": 0.5,
                              "y": 0.9,
                              "student_test_id": 12
                          },
                          {
                              "id": 2,
                              "name": "Петров П.П.",
                              "x": 0.65,
                              "y": 0.1,
                              "student_test_id": 122
                          }
                      ]
              },
              person_role_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      role_id: {type: :integer},
                      person_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "role_id": 12,
                      "person_id": 1
                  }
              },
              send_person_role_map: {
                  type: 'object',
                  properties: {
                      role_id: {type: :integer},
                      person_id: {type: :integer}
                  },
                  required: ['person_id', 'role_id'],
                  example: {
                      "role_id": 12,
                      "person_id": 1,

                  }
              },
              person_roles_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          role_id: {type: :integer},
                          person_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "role_id": 12,
                              "person_id": 1
                          },
                          {
                              "id": 2,
                              "role_id": 123,
                              "person_id": 11
                          }
                      ]
              },
              person_characteristic_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      characteristic_id: {type: :integer},
                      person_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "characteristic_id": 12,
                      "person_id": 1
                  }
              },
              send_person_characteristic_map: {
                  type: 'object',
                  properties: {
                      characteristic_id: {type: :integer},
                      person_id: {type: :integer}
                  },
                  required: ['person_id', 'characteristic_id'],
                  example: {
                      "characteristic_id": 12,
                      "person_id": 1
                  }
              },
              person_characteristics_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          characteristic_id: {type: :integer},
                          person_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "characteristic_id": 12,
                              "person_id": 1
                          },
                          {
                              "id": 2,
                              "characteristic_id": 123,
                              "person_id": 11
                          }
                      ]
              },
              antonym_map: {
                  type: 'object',
                  properties: {
                      first_char_id: {type: :integer},
                      second_char_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "first_char_id": 12,
                      "second_char_id": 1
                  }
              },
              send_antonym_map: {
                  type: 'object',
                  properties: {
                      first_char_id: {type: :integer},
                      second_char_id: {type: :integer}
                  },
                  required: ['first_char_id', 'second_char_id'],
                  example: {
                      "first_char_id": 12,
                      "second_char_id": 1
                  }
              },
              antonyms_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          first_char_id: {type: :integer},
                          second_char_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "first_char_id": 12,
                              "second_char_id": 1
                          },
                          {
                              "id": 2,
                              "first_char_id": 123,
                              "second_char_id": 11
                          }
                      ]
              },
              student_test_result_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      first_part_result: {
                          type: 'object',
                          properties: {
                              older_people: {type: :integer},
                              peers: {type: :integer},
                              male: {type: :integer},
                              female: {type: :integer},
                              relatives: {type: :integer},
                              study_place: {type: :integer},
                              real_comm: {type: :integer},
                              inet_com: {type: :integer},
                              fict_chars: {type: :integer},
                              famous: {type: :integer},
                              friends: {type: :integer},
                              study: {type: :integer},
                              sport: {type: :integer},
                              science: {type: :integer},
                              politics: {type: :integer},
                              army: {type: :integer},
                              art: {type: :integer},
                              business: {type: :integer},
                              empty: {type: :string},
                              max_categories: {type: :string},
                              act_areas: {type: :string}
                          }
                      },
                      second_part_result_id: {
                          type: :object,
                          properties: {
                              leading_values: {type: :string},
                              rejected: {type: :string},
                              personal_resources: {type: :string}
                          }
                      },
                      third_part_result_id: {
                          type: :object,
                          properties: {
                              optimist: {type: :string},
                              self_esteem: {type: :string},
                              equivalent: {type: :string},
                              ideals: {type: :string},
                              antiideals: {type: :string}
                          }
                      }

                  },
                  example: {
                      "id": 1,
                      "first_part_result": {
                          "older_people": 28,
                          "peers": 28,
                          "male": 28,
                          "female": 14,
                          "relatives": 21,
                          "study_place": 7,
                          "real_comm": 28,
                          "inet_com": 7,
                          "fict_chars": 28,
                          "famous": 42,
                          "friends": 0,
                          "study": 7,
                          "sport": 0,
                          "science": 7,
                          "politics": 7,
                          "army": 7,
                          "art": 14,
                          "business": 3,
                          "empty": "Спорт, Наука",
                          "max_categories": "мужской пол, старше, не старше",
                          "act_areas": "шоу-бизнес, искусство, литература, наука"
                      },
                      "second_part_result": {
                          "leading_values": "гедонические: удовольствие, глорические: успешный",
                          "rejected": "",
                          "personal_resources": ""
                      },
                      "third_part_result": {
                          "optimist": "Часто ты критически смотришь на все, что происходит",
                          "self_esteem": "Самооценка высокая",
                          "equivalent": "Ты чувствуешь свою индивидуальность, но некоторых воспринимаешь так же, как себя.",
                          "ideals": "Ты умеешь видеть лучшее в людях",
                          "antiideals": "В твоей жизни встречается много препятствий и проблем. Твои «антиидеалы»:Не стоит преодолевать их в одиночку. Надо объединиться с теми, кому ты доверяешь."
                      }
                  }
              },
              antonym_person_value_group_map: {
                  type: 'object',
                  properties: {
                      first_group_id: {type: :integer},
                      second_group_id: {type: :integer}
                  },
                  example: {
                      "id": 1,
                      "first_group_id": 12,
                      "second_group_id": 1
                  }
              },
              send_antonym_person_value_group_map: {
                  type: 'object',
                  properties: {
                      first_group_id: {type: :integer},
                      second_group_id: {type: :integer}
                  },
                  required: ['first_group_id', 'second_group_id'],
                  example: {
                      "first_group_id": 12,
                      "second_group_id": 1
                  }
              },
              antonym_person_value_groups_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id: {type: :integer},
                          first_group_id: {type: :integer},
                          second_group_id: {type: :integer}
                      },
                  },
                  example:
                      [
                          {
                              "id": 1,
                              "first_group_id": 12,
                              "second_group_id": 1
                          },
                          {
                              "id": 2,
                              "first_group_id": 123,
                              "second_group_id": 11
                          }
                      ]
              },

              test_result_map: {
                  type: 'object',
                  properties: {
                      id: {type: :integer},
                      hist_roles: {
                          type: :array,
                          items: {
                              properties: {
                                  role_name: {type: :string},
                                  generally: {type: :integer},
                                  indicate: {type: :integer},
                                  not_indicate: {type: :integer},
                                  indicate_procent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  not_indicate_procent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      hist_personal_value_groups: {
                          type: 'array',
                          items: {
                              properties: {
                                  group_name: {type: :string},
                                  group_human_name: {type: :string},
                                  value: {type: :integer},
                                  indicate: {type: :integer},
                                  not_indicate: {type: :integer},
                                  indicate_percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  not_indicate_percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      pair_roles: {
                          type: 'array',
                          items: {
                              properties: {
                                  first_role: {type: :string},
                                  second_role: {type: :string},
                                  eq_count: {type: :integer},
                                  lt_count: {type: :integer},
                                  gt_count: {type: :integer},
                                  eq_percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  lt_percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  gt_percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      hist_roles_groups: {
                          type: 'array',
                          items: {
                              properties: {
                                  role: {type: :string},
                                  personal_value_group: {type: :string},
                                  value: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      hist_group_roles: {
                          type: 'array',
                          items: {
                              properties: {
                                  role: {type: :string},
                                  personal_value_group: {type: :string},
                                  value: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      authority_roles: {
                          type: 'array',
                          items: {
                              properties: {
                                  value: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  role_name: {type: :string},
                                  valuation_profile_name: {type: :string}
                              }
                          }
                      },
                      soc_portraits: {
                          type: 'array',
                          items: {
                              properties: {
                                  roles: {type: :string},
                                  characteristics: {type: :string},
                                  groups: {type: :string},
                                  valuation_profile_name: {type: :string}
                              }
                          }
                      },
                      corr_terminal_instrumental_values: {
                          type: 'array',
                          items: {
                              properties: {
                                  valuation_profile_name: {type: :string},
                                  respondents_number: {type: :integer},
                                  without_respondents_number: {type: :integer},
                                  frequency: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  }
                              }
                          }
                      },
                      hist_self_esteems: {
                          type: 'array',
                          items: {
                              properties: {
                                  self_esteem_type_name: {type: :string},
                                  percent: {
                                      type: :number,
                                      minimum: 0,
                                      maximum: 1
                                  },
                                  value: {type: :integer}
                              }
                          }
                      }
                  },
                  example: {
                      "id": 1,
                      "hist_roles": [
                          {
                              "role_name": "родственники",
                              "generally": 1,
                              "indicate": 1,
                              "indicate_procent": 1,
                              "not_indicate": 0,
                              "not_indicate_procent": 0
                          },
                          {
                              "role_name": "наука",
                              "generally": 1,
                              "indicate": 1,
                              "indicate_procent": 1,
                              "not_indicate": 0,
                              "not_indicate_procent": 0
                          }
                      ],
                      "hist_personal_value_groups": [
                          {
                              "group_name": "физический облик",
                              "group_human_name": "Ценности физического совершенства",
                              "value": 1,
                              "indicate": 1,
                              "not_indicate": 0,
                              "indicate_percent": 1,
                              "not_indicate_percent": 0
                          },
                          {
                              "group_name": "манеры и нормы",
                              "group_human_name": "Ценности порядка и нормативности",
                              "value": 0,
                              "indicate": 0,
                              "not_indicate": 1,
                              "indicate_percent": 0,
                              "not_indicate_percent": 1
                          }
                      ],
                      "pair_roles": [
                          {
                              "first_role": "старше",
                              "second_role": "не старше",
                              "eq_count": 1,
                              "eq_percent": 1,
                              "lt_count": 0,
                              "lt_percent": 0,
                              "gt_count": 0,
                              "gt_percent": 0
                          },
                          {
                              "first_role": "мужской пол",
                              "second_role": "женский пол",
                              "eq_count": 0,
                              "eq_percent": 0,
                              "lt_count": 0,
                              "lt_percent": 0,
                              "gt_count": 1,
                              "gt_percent": 1
                          }
                      ],
                      "hist_role_groups": [
                          {
                              "role": "старше",
                              "personal_value_group": "гедонические",
                              "value": 0.16666666666666666
                          },
                          {
                              "role": "мужской пол",
                              "personal_value_group": "пугнические",
                              "value": 0
                          }
                      ],
                      "hist_group_roles": [
                          {
                              "role": "старше",
                              "personal_value_group": "гедонические",
                              "value": 0.125
                          },
                          {
                              "role": "мужской пол",
                              "personal_value_group": "пугнические",
                              "value": 0.5
                          }
                      ],
                      "authority_roles": [
                          {
                              "role_name": "старше",
                              "valuation_profile_name": "идеалы",
                              "value": 0.33
                          },
                          {
                              "role": "родители",
                              "valuation_profile_name": "антиидеалы",
                              "value": 0.5
                          }
                      ],
                      "soc_portraits": [
                          {
                              "roles": "не старше, старше, армия, политика",
                              "characteristics": "успешный, удовольствие",
                              "groups": "глорические, гедонические",
                              "valuation_profile_name": "идеалы"
                          },
                          {
                              "roles": "не старше, герои книг, фильмов, старше, искусство, литература, женский пол, интернет-знакомые, мужской пол, работники школы(вуза), шоу-бизнес, родители",
                              "characteristics": "умелый, общительный, смелый, добрый, умный, удовольствие, успешный, красивый",
                              "groups": "практические, коммуникативные, пугнические, альтруистические, гностические, гедонические, глорические, эстетические",
                              "valuation_profile_name": "хуже меня"
                          }
                      ],
                      "corr_terminal_instrumental_values": [
                          {
                              "valuation_profile_name": "идеалы",
                              "respondents_number": 0,
                              "without_respondents_number": 0,
                              "frequency": 0.15384615384615385
                          },
                          {
                              "valuation_profile_name": "хуже меня",
                              "respondents_number": 1,
                              "without_respondents_number": 0,
                              "frequency": 0.5384615384615384
                          }
                      ],
                      "hist_self_esteems": [
                          {
                              "self_esteem_type_name": "Высокая",
                              "percent": 0.33,
                              "value": 1
                          },
                          {
                              "self_esteem_type_name": "Низкая",
                              "percent": 0.66,
                              "value": 2
                          }
                      ],
                  }
              },
              send_user_map: {
                  type: 'object',
                  properties: {
                      login: { type: :string },
                      password: { type: :string },
                      school_id: {type: :integer},
                      type: {type: :string},
                  },
                  required: ['login', 'password', 'type'],
                  example:
                      {
                          login: 'admin1',
                          password: 'password1',
                          type: 'Admin'
                      }
              },
              user_map: {
                  type: 'object',
                  properties:{
                      id:  { type: :integer },
                      login: { type: :string },
                      school_id: {type: :integer},
                      type: {type: :string}
                  },
                  required: [ 'id', 'login', 'type'],
                  example: {
                      id: 1,
                      login: 'psi',
                      type: 'Psychologist'
                  }
              },
              users_map: {
                  type: 'array',
                  items: {
                      properties: {
                          id:  { type: :integer },
                          login: { type: :string },
                          school_id: {type: :integer},
                          student_id: {type: :integer},
                          type: {type: :string}
                      },
                      example: [
                          {
                              id: 1,
                              login: 'admin1',
                              password: 'password1',
                              type: 'Admin'
                          },
                          {
                              id: 2,
                              login: 'psi1',
                              password: 'password2',
                              type: 'Psychologist'
                          },
                          {
                              id: 3,
                              login: 'school_admin1',
                              password: 'password3',
                              school_id: 1,
                              type: 'SchoolAdmin'
                          },
                          {
                              id: 4,
                              login: 'school_psi',
                              password: 'password4',
                              school_id: 3,
                              type: 'SchoolPsyhologist'
                          },
                          {
                              id: 5,
                              login: 'student1',
                              password: 'password5',
                              student_id: 33,
                              type: 'StudentUser'
                          }
                      ]
                  }
              },
              NotFound: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Ресурс не найден']
                  }
              },

              Forbidden: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Неавторизованный запрос']
                  }
              },

              UniqOrIntegrity: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Нарушение уникальности']
                  }
              },
              InvalidToken: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Неверный токен']
                  }
              },
              StudentAlreadyExist: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Вы уже зарегистрированы.']
                  }
              },
              InvalidLoginOrPassword: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Неверный логин или пароль.']
                  }
              },
              SessionNotFound: {
                  type: 'object',
                  properties: {
                      errors: {type: :array}
                  },
                  example: {
                      errors: ['Ресурс не найден.']
                  }
              }
          }
      }
  }
end

