require 'swagger_helper'

describe 'Cities API' do
  path '/cities/{id}' do

    get 'Retrieves a city' do
      tags 'Cities'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'City found' do
        schema '$ref' => '#/definitions/city_map' 
	run_test!
      end
      response '404', 'City not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a city' do
      tags 'Cities'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :city, in: :body, 
        schema: {"$ref" => '#/definitions/send_city_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The city update' do
        schema '$ref' => '#/definitions/city_map' 
	run_test!
      end
      response '404', 'City not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a city' do
      tags 'Cities'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'City not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/cities' do
    post 'Create a city' do
      tags 'Cities'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :city, in: :body, 
        schema: {"$ref" => '#/definitions/send_city_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created city' do
        schema '$ref' => '#/definitions/city_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end
    get 'Retrieves all city' do
      tags 'Cities'
      produces 'application/json'
      parameter name: "filterrific[with_name]", :in => 'query', :type => :string,
                required: false, :description => "Substring of name"

      response '200', 'City found' do
        schema '$ref' => '#/definitions/cities_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
