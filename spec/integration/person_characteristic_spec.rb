require 'swagger_helper'

describe 'PersonCharacteristic API' do
  path '/person_characteristics/{id}' do

    get 'Retrieves a person characteristic' do
      tags 'PersonCharacteristics'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Person characteristic found' do
        schema '$ref' => '#/definitions/person_characteristic_map' 
	run_test!
      end
      response '404', 'Person characteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a person characteristic' do
      tags 'PersonCharacteristics'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person_characteristic, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_characteristic_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The person characteristic update' do
        schema '$ref' => '#/definitions/person_characteristic_map' 
	run_test!
      end
      response '404', 'Person characteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a person_characteristic' do
      tags 'PersonCharacteristics'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'PersonCharacteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/person_characteristics' do
    post 'Create a person_characteristic' do
      tags 'PersonCharacteristics'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person_characteristic, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_characteristic_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created person_characteristic' do
        schema '$ref' => '#/definitions/person_characteristic_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all person_characteristic' do
      tags 'PersonCharacteristics'
      produces 'application/json'
      parameter name: "filterrific[with_person_id]", :in => 'query', :type => :integer,
                required: false, :description => "Person id"
      parameter name: "filterrific[with_characteristic_id]", :in => 'query', :type => :integer,
                required: false, :description => "Characteristic id"

      response '200', 'PersonCharacteristic found' do
        schema '$ref' => '#/definitions/person_characteristics_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
