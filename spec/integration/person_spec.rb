require 'swagger_helper'

describe 'Person API' do
  path '/persons/{id}' do

    get 'Retrieves a person' do
      tags 'Persons'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Person found' do
        schema '$ref' => '#/definitions/person_map' 
	run_test!
      end
      response '404', 'Person not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a person' do
      tags 'Persons'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The person update' do
        schema '$ref' => '#/definitions/person_map' 
	run_test!
      end
      response '404', 'Person not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a person' do
      tags 'Persons'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Person not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/persons' do
    post 'Create a person' do
      tags 'Persons'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created person' do
        schema '$ref' => '#/definitions/person_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all person' do
      tags 'Persons'
      produces 'application/json'
      parameter name: "filterrific[with_student_test_id]", :in => 'query', :type => :integer,
                required: false, :description => "Student test id"

      response '200', 'Person found' do
        schema '$ref' => '#/definitions/persons_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
