require 'swagger_helper'

describe 'Role API' do
  path '/roles/{id}' do

    get 'Retrieves a role' do
      tags 'Roles'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Role found' do
        schema '$ref' => '#/definitions/role_map' 
	run_test!
      end
      response '404', 'Role not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end
  end
  path '/roles' do
    get 'Retrieves all role' do
      tags 'Roles'
      produces 'application/json'
      parameter name: "filterrific[with_name]", :in => 'query', :type => :string,
                required: false, :description => "Substring of name"

      response '200', 'Role found' do
        schema '$ref' => '#/definitions/roles_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
