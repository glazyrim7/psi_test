require 'swagger_helper'

describe 'Student API' do
  path '/students/{id}' do

    get 'Retrieves a student' do
      tags 'Students'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Student found' do
        schema '$ref' => '#/definitions/student_map'
        run_test!
      end
      response '404', 'Student not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end

    put 'Updates a student' do
      tags 'Students'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student, in: :body,
                schema: {"$ref" => '#/definitions/send_student_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The student update' do
        schema '$ref' => '#/definitions/student_map'
        run_test!
      end
      response '404', 'Student not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity'
        run_test!
      end
    end

    delete 'Deletes a student' do
      tags 'Students'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
        run_test!
      end
      response '404', 'Student not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end
  end
  path '/students' do
    post 'Create a student' do
      tags 'Students'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student, in: :body,
                schema: {"$ref" => '#/definitions/send_student_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created student' do
        schema '$ref' => '#/definitions/student_map'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity'
        run_test!
      end
    end
    get 'Retrieves all student' do
      tags 'Students'
      produces 'application/json'
      parameter name: "filterrific[with_student_group_id]", :in => 'query', :type => :integer,
                required: false, :description => "Student group id"
      response '200', 'Student found' do
        schema '$ref' => '#/definitions/students_map'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end

  end
end
