require 'swagger_helper'

describe 'School API' do
  path '/schools/{id}' do

    get 'Retrieves a school' do
      tags 'Schools'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'School found' do
        schema '$ref' => '#/definitions/school_map' 
	run_test!
      end
      response '404', 'School not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a school' do
      tags 'Schools'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :school, in: :body, 
        schema: {"$ref" => '#/definitions/send_school_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The school update' do
        schema '$ref' => '#/definitions/school_map' 
	run_test!
      end
      response '404', 'School not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a school' do
      tags 'Schools'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'School not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/schools' do
    post 'Create a school' do
      tags 'Schools'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :school, in: :body, 
        schema: {"$ref" => '#/definitions/send_school_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created school' do
        schema '$ref' => '#/definitions/school_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all school' do
      tags 'Schools'
      produces 'application/json'
      parameter name: "filterrific[with_name]", :in => 'query', :type => :string,
                required: false, :description => "Substring of name"
      parameter name: "filterrific[with_city_id]", :in => 'query', :type => :integer,
                required: false, :description => "City id"
      response '200', 'School found' do
        schema '$ref' => '#/definitions/schools_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
