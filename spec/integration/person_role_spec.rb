require 'swagger_helper'

describe 'PersonRole API' do
  path '/person_roles/{id}' do

    get 'Retrieves a person role' do
      tags 'PersonRoles'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Person role found' do
        schema '$ref' => '#/definitions/person_role_map' 
	run_test!
      end
      response '404', 'Person role not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a person role' do
      tags 'PersonRoles'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person_role, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_role_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The person role update' do
        schema '$ref' => '#/definitions/person_role_map' 
	run_test!
      end
      response '404', 'Person role not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a person_role' do
      tags 'PersonRoles'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'PersonRole not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/person_roles' do
    post 'Create a person_role' do
      tags 'PersonRoles'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :person_role, in: :body, 
        schema: {"$ref" => '#/definitions/send_person_role_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created person_role' do
        schema '$ref' => '#/definitions/person_role_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all person_role' do
      tags 'PersonRoles'
      produces 'application/json'
      parameter name: "filterrific[with_person_id]", :in => 'query', :type => :integer,
                required: false, :description => "Person id"
      parameter name: "filterrific[with_role_id]", :in => 'query', :type => :integer,
                required: false, :description => "Role id"

      response '200', 'PersonRole found' do
        schema '$ref' => '#/definitions/person_roles_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
