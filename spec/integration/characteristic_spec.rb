require 'swagger_helper'

describe 'Characteristic API' do
  path '/characteristics/{id}' do

    get 'Retrieves a characteristic' do
      tags 'Characteristics'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Characteristic found' do
        schema '$ref' => '#/definitions/characteristic_map' 
	run_test!
      end
      response '404', 'Characteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a characteristic' do
      tags 'Characteristics'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :characteristic, in: :body, 
        schema: {"$ref" => '#/definitions/send_characteristic_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The characteristic update' do
        schema '$ref' => '#/definitions/characteristic_map' 
	run_test!
      end
      response '404', 'Characteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a characteristic' do
      tags 'Characteristics'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Characteristic not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/characteristics' do
    post 'Create a characteristic' do
      tags 'Characteristics'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :characteristic, in: :body, 
        schema: {"$ref" => '#/definitions/send_characteristic_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created characteristic' do
        schema '$ref' => '#/definitions/characteristic_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all characteristic' do
      tags 'Characteristics'
      produces 'application/json'
      parameter name: "filterrific[with_name]", :in => 'query', :type => :string,
                required: false, :description => "Substring of name"

      response '200', 'Characteristic found' do
        schema '$ref' => '#/definitions/characteristics_map'
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
