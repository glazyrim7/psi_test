require 'swagger_helper'

describe 'Student group API' do
  path '/student_groups/{id}' do

    get 'Retrieves a student group' do
      tags 'Student groups'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Student group found' do
        schema '$ref' => '#/definitions/student_group_map' 
	run_test!
      end
      response '404', 'Student group not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a student group' do
      tags 'Student groups'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student_group, in: :body, 
        schema: {"$ref" => '#/definitions/send_student_group_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The student group update' do
        schema '$ref' => '#/definitions/student_group_map' 
	run_test!
      end
      response '404', 'Student group not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a student group' do
      tags 'Student groups'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Student group not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/student_groups' do
    post 'Create a student group' do
      tags 'Student groups'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student_group, in: :body, 
        schema: {"$ref" => '#/definitions/send_student_group_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created student group' do
        schema '$ref' => '#/definitions/student_group_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all student group' do
      tags 'Student groups'
      produces 'application/json'
      parameter name: "filterrific[with_school_id]", :in => 'query', :type => :integer,
                required: false, :description => "School id"

      response '200', 'Student group found' do
        schema '$ref' => '#/definitions/student_groups_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
