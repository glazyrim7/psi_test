require 'swagger_helper'

describe 'Auth API' do
  path '/auth/sign_in' do

    post 'Sign in' do
      tags 'Auth'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
          type: :object,
          properties: {
              login: { type: :string },
              password: { type: :string },
          },
          required: [ 'login', 'password'],
          example: {
              login: 'test_user',
              password: 'password',
          }
      }
      response '200', 'Auth success' do
        header 'access-token', type: :string, description: 'Access token'
        header 'client', type: :string, description: 'Client'
        header 'uid', type: :string, description: 'User id'
        schema '$ref' => '#/definitions/user_map'
        run_test!
      end
      response '403', 'Invalid login or password' do
        schema '$ref' => '#/definitions/InvalidLoginOrPassword'
        run_test!
      end
    end
  end
  path '/auth/sign_out' do
    delete 'Sign out' do
      tags 'Auth'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
          type: :string,
      }
      parameter name: 'client', in: :header, schema: {
          type: :string
      }
      parameter name: 'uid', in: :header, schema: {
          type: :string
      }
      response '204', 'Success' do
        run_test!
      end
      response '404', 'Session not found' do
        schema '$ref' => '#/definitions/SessionNotFound'
        run_test!
      end
    end
  end

  path '/auth/validate_token' do
    get 'Validate token' do
      tags 'Auth'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
          type: :string,
      }
      parameter name: 'client', in: :header, schema: {
          type: :string
      }
      parameter name: 'uid', in: :header, schema: {
          type: :string
      }
      response '200', 'Success' do
        run_test!
      end
      response '401', 'Invalid token' do
        run_test!
      end
    end
  end
end
