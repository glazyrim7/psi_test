require 'swagger_helper'

describe 'Test API' do
  path '/tests/{id}' do

    get 'Retrieves a test' do
      tags 'Tests'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Test found' do
        schema '$ref' => '#/definitions/test_map' 
	run_test!
      end
      response '404', 'Test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a test' do
      tags 'Tests'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :test, in: :body, 
        schema: {"$ref" => '#/definitions/send_test_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The test update' do
        schema '$ref' => '#/definitions/test_map' 
	run_test!
      end
      response '404', 'Test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a test' do
      tags 'Tests'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/tests' do
    post 'Create a test' do
      tags 'Tests'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :test, in: :body, 
        schema: {"$ref" => '#/definitions/send_test_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created test' do
        schema '$ref' => '#/definitions/test_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all tests' do
      tags 'Tests'
      produces 'application/json'
      parameter name: "filterrific[with_student_group_id]", :in => 'query', :type => :integer,
                required: false, :description => "Student group id"
      parameter name: "filterrific[active]", :in => 'query', :type => :integer,
                required: false, :description => "Active tests"
      parameter name: "filterrific[with_student_id]", :in => 'query', :type => :integer,
                required: false, :description => "Tests for student"
      parameter name: "filterrific[with_school_id]", :in => 'query', :type => :integer,
                required: false, :description => "Tests for school"
      response '200', 'Test found' do
        schema '$ref' => '#/definitions/tests_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
  path '/tests/{id}/result' do
    get 'Retrieves test result' do
      tags 'Tests'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer
      response '200', 'Test result found' do
        schema '$ref' => '#/definitions/test_result_map'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end
  end
end
