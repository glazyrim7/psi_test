require 'swagger_helper'

describe 'User API' do
  path '/users/{id}' do

    get 'Retrieves a user' do
      tags 'Users'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'User found' do
        schema '$ref' => '#/definitions/user_map'
        run_test!
      end
      response '404', 'User not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end

    put 'Updates the user' do
      tags 'Users'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body,
                schema: {"$ref" => '#definitions/send_user_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The user updated' do
        schema '$ref' => '#/definitions/user_map'
        run_test!
      end
      response '404', 'User not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity'
        run_test!
      end
    end

    delete 'Deletes the user' do
      tags 'Users'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
        run_test!
      end
      response '404', 'User not found' do
        schema '$ref' => '#/definitions/NotFound'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end
  end
  path '/users' do
    post 'Create a user' do
      tags 'Users'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body,
                schema: {"$ref" => '#definitions/send_user_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created user' do
        schema '$ref' => '#/definitions/user_map'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity'
        run_test!
      end
    end
    get 'Retrieves all users' do
      tags 'Users'
      produces 'application/json'

      parameter name: "type", :in => 'query', :type => :string, required: false, :description => "User type"
      parameter name: "filterrific[with_login]", :in => 'query', :type => :string,
                required: false, :description => "Substring of login"

      response '200', 'Users found' do
        schema '$ref' => '#/definitions/users_map'
        run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
        run_test!
      end
    end



  end
end

