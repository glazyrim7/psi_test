require 'swagger_helper'

describe 'Personal value group API' do
  path '/personal_value_groups/{id}' do

    get 'Retrieves a personal value group' do
      tags 'PersonalValueGroups'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Personal value group found' do
        schema '$ref' => '#/definitions/personal_value_group_map'
	      run_test!
      end
      response '404', 'Personal value group not found' do
        schema '$ref' => '#/definitions/NotFound'
	      run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden'
	      run_test!
      end
    end
  end
  path '/personal_value_groups' do
    get 'Retrieves all personal value groups' do
      tags 'PersonalValueGroups'
      produces 'application/json'
      parameter name: "filterrific[with_name]", :in => 'query', :type => :string,
                required: false, :description => "Substring of name"

      response '200', 'Personal values groups found' do
        schema '$ref' => '#/definitions/personal_value_groups_map' 
	      run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	      run_test!
      end
    end

  end
end
