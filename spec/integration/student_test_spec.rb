require 'swagger_helper'

describe 'Student test API' do
  path '/student_tests/{id}' do

    get 'Retrieves a student test' do
      tags 'Student tests'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Student test found' do
        schema '$ref' => '#/definitions/student_test_map' 
	run_test!
      end
      response '404', 'Student test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

    put 'Updates a student test' do
      tags 'Student tests'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student_test, in: :body, 
        schema: {"$ref" => '#/definitions/send_student_test_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'The student test update' do
        schema '$ref' => '#/definitions/student_test_map' 
	run_test!
      end
      response '404', 'Student test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 

    delete 'Deletes a student test' do
      tags 'Student tests'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'Success' do
	run_test!
      end
      response '404', 'Student test not found' do
        schema '$ref' => '#/definitions/NotFound' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end 
  end
  path '/student_tests' do
    post 'Create a student test' do
      tags 'Student tests'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :student_test, in: :body, 
        schema: {"$ref" => '#/definitions/send_student_test_map'}
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Created student test' do
        schema '$ref' => '#/definitions/student_test_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
      response '409', 'Violation of uniqueness or integrity' do
        schema '$ref' => '#/definitions/UniqOrIntegrity' 
	run_test!
      end
    end 
    get 'Retrieves all student test' do
      tags 'Student tests'
      produces 'application/json'
      parameter name: "filterrific[with_student_id]", :in => 'query', :type => :integer,
                required: false, :description => "Student id"
      parameter name: "filterrific[with_test_id]", :in => 'query', :type => :integer,
                required: false, :description => "Test id"

      response '200', 'Student test found' do
        schema '$ref' => '#/definitions/student_tests_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end

  path '/student_tests/{id}/result' do
     get 'Retrieves student test result' do
      tags 'Student tests'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Student test result found' do
        schema '$ref' => '#/definitions/student_test_result_map' 
	run_test!
      end
      response '401', 'Unauthorized' do
        schema '$ref' => '#/definitions/Forbidden' 
	run_test!
      end
    end

  end
end
