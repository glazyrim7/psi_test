require 'swagger_helper'

describe 'Registration API' do
  path '/sign_up' do

    post 'Sign up' do
      tags 'Registration'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
          type: :object,
          properties: {
              login: { type: :string },
              password: { type: :string },
              token: {type: :string}
          },
          required: [ 'login', 'password', 'token'],
          example: {
              login: 'test_user',
              password: 'password',
              token: 'yua7saSA'
          }
      }
      response '200', 'Registration success' do
        schema '$ref' => '#/definitions/user_map'
        run_test!
      end
      response '403', 'Invalid token or student already exist' do
        schema '$ref' => '#/definitions/InvalidToken'
        run_test!
      end
    end
  end
end

