class StudentGroupsController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        StudentGroup,
        params[:filterrific],
        sanitize_params: false,
        )
    student_groups = filterrific.find
    render json: student_groups.select {|item| can?(:show, item) }, status: :ok
  end

  private

  def set_model
    @model = StudentGroup
  end

  def resource_params
    params.permit(:school_id, :letter, :form_date)
  end

end
