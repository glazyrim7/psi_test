require 'api_error'

class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  rescue_from CanCan::AccessDenied do |exception|
    forbidden
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    record_not_found
  end

  #rescue_from Exception, with: :internal_error
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActionController::RoutingError, with: :record_not_found

  def raise_not_found!
    raise ActionController::RoutingError, "No route matches #{params[:unmatched_route]}"
  end

  def can_not_save
    render json: ApiError.can_not_save, status: :internal_server_error 
  end

  def not_valid(e)
    render json: ApiError.new(e), status: :conflict
  end

  def record_not_found
    render json: ApiError.not_found, status: :not_found
  end

  def forbidden
    render json: ApiError.forbidden, status: :forbidden
  end

  def unsupported_get_auth
    render json: ApiError.get_not_supported_auth, status: :method_not_allowed
  end

  def bad_login
    render json: ApiError.login, status: :forbidden
  end

  def bad_request
    render json: ApiError.bad_request, status: :bad_request
  end

  def invalid_student_token
    render json: ApiError.invalid_student_token, status: :conflict
  end

  def student_user_already_exist
    render json: ApiError.student_user_already_exist, status: :conflict
  end

  def internal_error
    render json: ApiError.internal_error, status: :internal_server_error
  end

  def auth_user!
    unless current_user
      forbidden
    end
  end

end
