class CitiesController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        City,
        params[:filterrific],
        sanitize_params: false,
        )
    render json: filterrific.find, status: :ok
  end

  private

  def set_model
    @model = City 
  end

  def resource_params
    params.permit(:name)
  end
end
