class StudentTestsController < ApplicationController
  include CrudUWD
  before_action :set_resource, only: %i[show update destroy result]

  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        StudentTest,
        params[:filterrific],
        sanitize_params: false,
        )
    student_tests = filterrific.find
    render json: student_tests.select {|item| can?(:show, item)}, status: :ok
  end

  def result
    begin
      @res.analyze unless @res.student_test_result
      render json: @res.student_test_result, status: :ok
    rescue StandardError => e
      render json: ApiError.new([e.message]), status: :conflict
    end
  end

  private

  def set_model
    @model = StudentTest
  end

  def resource_params
    params.permit(:student_id, :test_id, persons_attributes: [
        :id, :name, :x, :y,
        person_roles_attributes: %i[
          id person_id role_id
        ],
        person_characteristics_attributes: %i[
          id person_id characteristic_id
        ]
    ])
  end

  def create_serializer
    StudentTestFullSerializer
  end

end
