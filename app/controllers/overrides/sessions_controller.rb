module Overrides
  class SessionsController < DeviseTokenAuth::SessionsController

    def provider
      'login'
    end

    def render_create_success
      render json: @resource
    end

    def render_new_error
      unsupported_get_auth
    end

    def render_destroy_success
      render :nothing => true, :status => 204
    end

    def render_destroy_error
      record_not_found
    end

    def render_create_error_bad_credentials
      bad_login
    end
  end
end

