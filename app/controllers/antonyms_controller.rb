class AntonymsController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        Antonym,
        params[:filterrific],
        sanitize_params: false,
        )
    render json: filterrific.find, status: :ok
  end

  private

  def set_model
    @model = Antonym 
  end

  def resource_params
    params.permit(:first_char_id, :second_char_id)
  end
end
