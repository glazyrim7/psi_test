class RegistrationController < ApplicationController
  def sign_up
    token = params[:token]
    if token.nil? || (student = Student.find_by(token: token)).nil?
      invalid_student_token
      return
    end

    if StudentUser.find_by(student_id: student.id)
      student_user_already_exist
      return
    end

    user = StudentUser.new(login: params[:login], password: params[:password],
                           student: student)

    unless user.valid?
      not_valid(user.errors.full_messages)
      return
    end

    if user.save
      render json: user, status: :ok
    else
      can_not_save
    end
  end

  private

  def resource_params
    params.permit(:token, :login, :password)
  end


end
