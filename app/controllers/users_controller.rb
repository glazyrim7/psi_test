class UsersController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    render json: @model.all, status: :ok
  end

  def update
    prms = resource_params
    if prms && prms['password'].blank?
      prms.delete('password')
    end
    @res.attributes = prms
    unless @res.valid?
      not_valid @res.errors.full_messages
      return
    end
    authorize! :update, @res
    if @res.save
      render json: @res, status: :ok
    else
      can_not_save
    end
  end

  private

  def set_model
    case params[:type]
    when 'Admin'
      @model = Admin
    when 'Psychologist'
      @model = Psychologist
    when 'SchoolAdmin'
      @model = SchoolAdmin
    when 'StudentUser'
      @model = StudentUser
    when 'SchoolPsychologist'
      @model = SchoolPsychologist
    else
      @model = User
    end
  end

  def admin_params
    params.permit(:login, :password)
  end

  def psychologist_params
    params.permit(:login, :password, :school_id)
  end

  def school_admin_params
    params.permit(:login, :password, :school_id)
  end

  def school_psychologist_params
    params.permit(:login, :password, :school_id)
  end

  def student_user_params
    params.permit(:login, :password, :student_group_id)
  end

  def resource_params
    send("#{@model.to_s.underscore.to_sym}_params")
  end

end
