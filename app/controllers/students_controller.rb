class StudentsController < ApplicationController
  include CrudUWD

  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        Student,
        params[:filterrific],
        sanitize_params: false,
        )
    students = filterrific.find
    render json: students.select {|item| can?(:show, item)}, status: :ok
  end

  private

  def set_model
    @model = Student
  end

  def resource_params
    params.permit(:student_group_id, :name, :sex)
  end
end
