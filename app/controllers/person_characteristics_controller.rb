class PersonCharacteristicsController < ApplicationController
  include CrudUWD
  load_and_authorize_resource

  def index
    filterrific = initialize_filterrific(
        PersonCharacteristic,
        params[:filterrific],
        sanitize_params: false,
        )
    person_chars = filterrific.find
    render json: person_chars.select {|item| can?(:show, item)}, status: :ok
  end

  private

  def set_model
    @model = PersonCharacteristic 
  end

  def resource_params
    params.permit(:person_id, :characteristic_id)
  end
end
