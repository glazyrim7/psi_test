class School < ApplicationRecord
  belongs_to :city
  validates :name, presence: true
  validates_uniqueness_of :name, scope: :city_id

  filterrific :available_filters => [
      :with_name,
      :with_city_id
  ]

  scope :with_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(name) LIKE ?", "%#{query}%".downcase)
  }

  scope :with_city_id, ->(id){
    where(:city_id => [*id])
  }
end
