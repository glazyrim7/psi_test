class SelfEsteemType < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: {case_sensitive: true}

  def self.high
    find_by(name: 'Высокая')
  end

  def self.normal
    find_by(name: 'Нормальная')
  end

  def self.reduced
    find_by(name: 'Сниженная')
  end

  def high?
    name == 'Высокая'
  end

  def normal?
    name == 'Нормальная'
  end

  def reduced?
    name == 'Сниженная'
  end

end
