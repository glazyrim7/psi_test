class HistPersonalValueGroup < ApplicationRecord
  belongs_to :test_result
  belongs_to :personal_value_group

  validates :value, :indicate_percent, :not_indicate_percent, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 1}
  validates :indicate, :not_indicate, numericality: {only_integer: true, greater_than_or_equal_to: 0}

end
