# frozen_string_literal: true

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :trackable, :authentication_keys => [:login]
  include DeviseTokenAuth::Concerns::User

  validates :login, presence: true
  validates_uniqueness_of :login

  before_validation do
    self.uid = login if uid.blank?
  end

  protected

  def token_validation_response
    UserSerializer.new(self, root:false)
  end

end
