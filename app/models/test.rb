class Test < ApplicationRecord
  belongs_to :student_group
  has_many :student_tests, dependent: :destroy
  belongs_to :test_result, optional: true, dependent: :destroy

  validates :start, :finish, presence: true
  validate :check_dates

  filterrific available_filters: %i[
      with_school_id
      with_student_group_id
      with_student_id
      active
  ]

  scope :with_student_group_id, lambda { |id|
    where(student_group_id: [*id])
  }

  scope :with_school_id, lambda { |id|
    where(student_groups: { school_id: id }).joins(:student_group)
  }

  scope :active, lambda { |value|
    if value
      if Rails.env.production?
        return where("start < now() AND finish > now()")
      else
        return where("start < datetime('now') AND finish > datetime('now')")
      end
    end
  }

  scope :with_student_id, lambda { |id|
    joins(student_group: :students).where(students: { id: id })
        .where([%(NOT EXISTS (SELECT 1 FROM "student_tests" WHERE "student_tests"."test_id" = "tests"."id" AND "student_tests"."student_id" = "students"."id"))])
  }

  def analyze
    @st = student_tests
    @st = @st.reject{|item| item.student_test_result.nil?}
    validate

    hrs = hist_role
    hgs = hist_group

    max_role = hrs.max_by {|item| item.generally}.role
    rps = role_pairs(max_role)

    roles_row, groups_row = hist_role_student_personal_value_groups
    ass = authority_roles
    soc_portraits = soc_portrait
    ctivs = corr_terminal_instrument_values
    hses = hist_self_esteems

    ActiveRecord::Base.transaction do
      tr = TestResult.create

      hrs.each { |item| item.test_result = tr }
      hrs.each(&:save)

      hgs.each { |item| item.test_result = tr }
      hgs.each(&:save)

      rps.each { |item| item.test_result = tr }
      rps.each(&:save)

      roles_row.each { |item| item.test_result = tr }
      roles_row.each(&:save)

      groups_row.each { |item| item.test_result = tr }
      groups_row.each(&:save)

      ass.each { |item| item.test_result = tr }
      ass.each(&:save)

      soc_portraits.each { |item| item.test_result = tr }
      soc_portraits.each(&:save)

      ctivs.each { |item| item.test_result = tr }
      ctivs.each(&:save)

      hses.each { |item| item.test_result = tr }
      hses.each(&:save)

      update_attributes(test_result: tr)

    end

  end

  def hist_self_esteems

    h = {}

    SelfEsteemType.all.each do |type|
      h[type] = 0
    end

    @st.each do |st|
      type = st.self_esteem_type
      if h.key?(type)
        h[type] += 1
      else
        h[type] = 1
      end
    end

    counter = h.values.sum

    h.map { |k, v| HistSelfEsteem.new(self_esteem_type: k, value: v, percent: !v.zero? ? v / counter.to_f : 0) }

  end

  private

  def validate
    raise StandardError, 'Никто не прошел тест.' if @st.empty?
    raise StandardError, 'Не все прошли тестирование.' if student_group.students.size > @st.size and finish > DateTime.now
  end

  def corr_terminal_instrument_values
    ideals_count = 0
    antiideals_count = 0
    toller_then_me_count = 0
    worse_then_me_count = 0
    like_me_count = 0

    not_ideals_count = 0
    not_antiideals_count = 0
    not_toller_then_me_count = 0
    not_worse_then_me_count = 0
    not_like_me_count = 0

    more_ideals_count = 0
    more_antiideals_count = 0
    more_toller_then_me_count = 0
    more_worse_then_me_count = 0
    more_like_me_count = 0

    @st.each do |st|
      st_ideals_count = st.ideals.size
      ideals_count += st_ideals_count
      not_ideals_count += 1 if st_ideals_count.zero?

      st_antiideals_count = st.antiideals.size
      antiideals_count += st_antiideals_count
      not_antiideals_count += 1 if st_antiideals_count.zero?

      st_toller_then_me_count = st.toller_then_me.size
      toller_then_me_count += st_toller_then_me_count
      not_toller_then_me_count += 1 if st_toller_then_me_count.zero?

      st_worse_then_me_count = st.worse_then_me.size
      worse_then_me_count += st_worse_then_me_count
      not_worse_then_me_count += 1 if st_worse_then_me_count.zero?

      st_like_me_count = st.like_me.size
      like_me_count += st_like_me_count
      not_like_me_count += 1 if st_like_me_count.zero?

      more_ideals_count += 1 if st_like_me_count > [st_antiideals_count, st_toller_then_me_count,
                                                    st_worse_then_me_count, like_me_count].max

      more_antiideals_count += 1 if st_antiideals_count > [st_ideals_count, st_toller_then_me_count,
                                                           st_worse_then_me_count, like_me_count].max

      more_toller_then_me_count += 1 if st_toller_then_me_count > [st_ideals_count, st_antiideals_count,
                                                                   st_worse_then_me_count, like_me_count].max

      more_worse_then_me_count += 1 if st_worse_then_me_count > [st_ideals_count, st_antiideals_count,
                                                                 st_toller_then_me_count, like_me_count].max

      more_like_me_count += 1 if st_like_me_count > [st_ideals_count, st_antiideals_count,
                                                     st_toller_then_me_count, st_worse_then_me_count].max

    end
    count = ideals_count + antiideals_count + toller_then_me_count + worse_then_me_count + like_me_count

    [CorrTerminalInstrumentalValue.new(voluation_profile: VoluationProfile.ideals,
                                       frequency: ideals_count.to_f / count,
                                       without_respondents_number: not_ideals_count,
                                       respondents_number: more_ideals_count),
     CorrTerminalInstrumentalValue.new(voluation_profile: VoluationProfile.antiideals,
                                       frequency: antiideals_count.to_f / count,
                                       without_respondents_number: not_antiideals_count,
                                       respondents_number: more_antiideals_count),
     CorrTerminalInstrumentalValue.new(voluation_profile: VoluationProfile.toller_then_me,
                                       frequency: toller_then_me_count.to_f / count,
                                       without_respondents_number: not_toller_then_me_count,
                                       respondents_number: more_toller_then_me_count),
     CorrTerminalInstrumentalValue.new(voluation_profile: VoluationProfile.worse_then_me,
                                       frequency: worse_then_me_count.to_f / count,
                                       without_respondents_number: not_worse_then_me_count,
                                       respondents_number: more_worse_then_me_count),
     CorrTerminalInstrumentalValue.new(voluation_profile: VoluationProfile.like_me,
                                       frequency: like_me_count.to_f / count,
                                       without_respondents_number: not_like_me_count,
                                       respondents_number: more_like_me_count)]
  end

  def soc_portrait
    res_hash = {}
    @st.each do |item|
      item_res = item.soc_portrait_freqs
      item_res.each do |voluation_key, voluation_value|
        if res_hash.key?(voluation_key)
          voluation_value.each do |part_key, part_value|
            part_value.each do |freq_key, freq_value|
              if res_hash[voluation_key][part_key].key?(freq_key)
                res_hash[voluation_key][part_key][freq_key] += freq_value
              else
                res_hash[voluation_key][part_key][freq_key] = freq_value
              end
            end
          end
        else
          res_hash[voluation_key] = voluation_value
        end
      end
    end

    res = []

    res_hash.each do |voluation_key, voluation_value|
      voluation_value.each do |part_key, part_value|
        count = res_hash[voluation_key][part_key].map { |item| item[1] }.sum
        res_hash[voluation_key][part_key] = part_value.to_a.sort_by { |item| -item[1] }.map { |item| item[0] +
            '(' + (item[1].to_f * 100 / count).round.to_s + '%)' }.join(', ')
      end
      res.append(SocPortrait.new(voluation_profile: voluation_key, roles: res_hash[voluation_key][:roles],
                                 characteristics: res_hash[voluation_key][:characteristics],
                                 groups: res_hash[voluation_key][:groups]))
    end
    res
  end

  def authority_roles_by_persons(ars, persons, vp)
  persons.each do |i|
    i.person_roles.each do |pr|
      ars[[pr.role, vp]].value += 1
    end
  end
  end

  def authority_roles
    ars = {}
    Role.all.each do |r|
      VoluationProfile.all.each do |v|
        ars[[r, v]] = AuthorityRole.new(value: 0, role: r, voluation_profile: v)
      end
    end

    @st.each do |item|
      authority_roles_by_persons(ars, item.ideals, VoluationProfile.ideals)
      authority_roles_by_persons(ars, item.antiideals, VoluationProfile.antiideals)
      authority_roles_by_persons(ars, item.toller_then_me, VoluationProfile.toller_then_me)
      authority_roles_by_persons(ars, item.worse_then_me, VoluationProfile.worse_then_me)
      authority_roles_by_persons(ars, item.like_me, VoluationProfile.like_me)
    end

    count_in_row = {}
    Role.all.each do |r|
      count_in_row[r] = 0
      VoluationProfile.all.each do |v|
        count_in_row[r] += ars[[r, v]].value
      end
    end

    Role.all.each do |r|
      VoluationProfile.all.each do |v|
        if count_in_row[r] != 0
          ars[[r, v]].value = ars[[r, v]].value.to_f / count_in_row[r]
        end
      end
    end

    ars.values
  end

  def hist_role
    hrs = {}
    Role.all.each do |r|
      hrs[r.name] = HistRole.new(generally: 0, indicate: 0, indicate_procent: 0,
                                 not_indicate: @st.size, not_indicate_procent: 1, role: r)
    end
    roles_count = 0
    @st.each do |item|
      item.roles_hash.each_pair do |k, v|
        hrs[k].generally += v
        hrs[k].indicate += 1
        hrs[k].not_indicate -= 1
      end
      item.persons.each do |p|
        roles_count += p.person_roles.size
      end
    end

    res = hrs.values

    res.each do |item|
      item.generally = item.generally.to_f / roles_count
      item.indicate_procent = item.indicate.to_f / @st.size
      item.not_indicate_procent = item.not_indicate.to_f / @st.size
    end

    res
  end

  def hist_group
    hgs = {}
    PersonalValueGroup.all.each do |g|
      hgs[g.name] = HistPersonalValueGroup.new(value: 0, indicate: 0, indicate_percent: 0,
                                               not_indicate: @st.size, not_indicate_percent: 100, personal_value_group: g)
    end
    group_count = 0
    @st.each do |item|
      item.groups_hash.each_pair do |k, v|
        hgs[k].value += v
        hgs[k].indicate += 1
        hgs[k].not_indicate -= 1
      end
      item.persons.each do |p|
        group_count += p.person_characteristics.size
      end
    end

    res = hgs.values

    res.each do |item|
      item.value = item.value.to_f / group_count
      item.indicate_percent = item.indicate.to_f / @st.size
      item.not_indicate_percent = item.not_indicate.to_f / @st.size
    end

    res
  end

  def role_pairs(max_role)
    arr = [
        PairRole.new(first_role: Role.older, second_role: Role.non_older, eq_count: 0, gt_count: 0, lt_count: 0, eq_percent: 0, gt_percent: 0, lt_percent: 0),
        PairRole.new(first_role: Role.male, second_role: Role.female, eq_count: 0, gt_count: 0, lt_count: 0, eq_percent: 0, gt_percent: 0, lt_percent: 0),
        PairRole.new(first_role: Role.par, second_role: max_role, eq_count: 0, gt_count: 0, lt_count: 0, eq_percent: 0, gt_percent: 0, lt_percent: 0),
        PairRole.new(first_role: Role.classmates, second_role: max_role, eq_count: 0, gt_count: 0, lt_count: 0, eq_percent: 0, gt_percent: 0, lt_percent: 0),
        PairRole.new(first_role: Role.kurgan_region, second_role: Role.abroad, eq_count: 0, gt_count: 0, lt_count: 0, eq_percent: 0, gt_percent: 0, lt_percent: 0)
    ]

    @st.each do |item|
      item_roles = item.roles_hash
      arr.each do |pr|
        key1 = pr.first_role.name
        key2 = pr.second_role.name
        count1 = item_roles.key?(key1) ? item_roles[key1] : 0
        count2 = item_roles.key?(key2) ? item_roles[key2] : 0
        if count1 == count2
          pr.eq_count += 1
        elsif count1 > count2
          pr.gt_count += 1
        else
          pr.lt_count += 1
        end
      end
    end

    arr.each do |pr|
      pr.eq_percent = pr.eq_count.to_f / @st.size
      pr.gt_percent = pr.gt_count.to_f / @st.size
      pr.lt_percent = pr.lt_count.to_f / @st.size
    end

    arr
  end

  def hist_role_student_personal_value_groups
    res_role = {}
    res_group = {}

    roles = Role.all
    groups = PersonalValueGroup.all

    roles_value = {}

    roles.each do |r|
      groups.each do |g|
        res_role[[r.id, g.id]] = HistRolePersonalValueGroup.new(role: r, personal_value_group: g, value: 0, direction: 'role_row')
        res_group[[r.id, g.id]] = HistRolePersonalValueGroup.new(role: r, personal_value_group: g, value: 0, direction: 'group_row')
      end
      roles_value[r.id] = 0
    end

    groups_value = {}

    groups.each do |g|
      groups_value[g.id] = 0
    end

    @st.each do |pr|
      item_res = pr.hist_role_personal_value_group_count
      item_res.each_pair do |k, v|
        res_role[k].value += v
        res_group[k].value += v

        roles_value[k[0]] += v
        groups_value[k[1]] += v
      end
    end

    res_role.each_pair do |k, v|
      v.value = v.value.to_f / (roles_value[k[0]] != 0 ? roles_value[k[0]] : 1)
    end

    res_group.each_pair do |k, v|
      v.value = v.value.to_f / (groups_value[k[1]] != 0 ? groups_value[k[1]] : 1)
    end

    [res_role.values, res_group.values]

  end


  def check_dates
    if start.present? && finish.present? && (start > finish || finish < Date.today)
      errors.add(:start, "Неверный период проведения тестирования")
    end
  end

end
