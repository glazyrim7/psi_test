class Characteristic < ApplicationRecord
  belongs_to :personal_value_group
  validates :name, :positivity, presence: true
  validates :name, uniqueness: {case_sensitive: true}
  validates :positivity, numericality: {greater_than_or_equal_to: -1, less_than_or_equal_to: 1}

  has_many :first_chars, class_name: "Antonym", foreign_key: "first_char_id"
  has_many :second_chars, class_name: "Antonym", foreign_key: "second_char_id"

  def is_positive?
    not positivity.nil? and positivity > 0 ? true : false
  end

  filterrific :available_filters => [
      :with_name
  ]

  scope :with_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(name) LIKE ?", "%#{query}%".downcase)
  }

end