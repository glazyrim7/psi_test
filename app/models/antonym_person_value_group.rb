class AntonymPersonValueGroup < ApplicationRecord
  belongs_to :first_group, class_name: "PersonalValueGroup"
  belongs_to :second_group, class_name: "PersonalValueGroup"

  filterrific available_filters: [
      :with_personal_value_group_id
  ]

  scope :with_personal_value_group_id, lambda { |id|
    where(first_group: [*id]).or(where(second_group: [*id]))
  }
end

