class Person < ApplicationRecord
  belongs_to :student_test

  has_many :person_roles, dependent: :destroy
  has_many :person_characteristics, dependent: :destroy

  accepts_nested_attributes_for :person_roles, allow_destroy: true
  accepts_nested_attributes_for :person_characteristics, allow_destroy: true

  validates :name, :x, :y, presence: true
  #validates_uniqueness_of :name, scope: :student_test_id

  filterrific :available_filters => [
      :with_student_test_id
  ]

  scope :with_student_test_id, ->(id){
    where(:student_test_id => [*id])
  }

end
