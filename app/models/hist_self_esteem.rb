class HistSelfEsteem < ApplicationRecord
  belongs_to :test_result
  belongs_to :self_esteem_type

  validates :percent, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1 }
  validates :value, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

end
