require 'securerandom'

class Student < ApplicationRecord
  belongs_to :student_group
  has_many :student_tests
  enum sex: {male: 1, female: 2}
  validates :name, :sex, presence: true

  filterrific :available_filters => [
      :with_student_group_id
  ]

  scope :with_student_group_id, ->(id){
    where(:student_group_id => [*id])
  }

  before_create do
    begin
      self.token = SecureRandom.alphanumeric(8)
    end while Student.find_by(token: token)
  end

end
