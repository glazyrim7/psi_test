class StudentTestResult < ApplicationRecord
  belongs_to :first_part_result, dependent: :destroy
  belongs_to :second_part_result, dependent: :destroy
  belongs_to :third_part_result, dependent: :destroy
end
