# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user.present?
      if user.type == 'Admin'
        can :manage, :all
        can :result, StudentTest
        can :result, Test
      elsif user.type == 'Psychologist'
        can :manage, Characteristic
        can %i[index show], [Person, City, PersonRole, PersonCharacteristic, PersonalValueGroup,
                             Role, School, StudentGroup, Student, Test, StudentTest]
        can :result, StudentTest
        can :result, Test
        can :show, User, id: user.id
      elsif user.type == 'SchoolAdmin'
        can %i[index show], [Characteristic, City, Role, School, PersonalValueGroup]
        can :manage, StudentGroup, school_id: user.school_id
        can :manage, Student do |s|
          s.student_group && s.student_group.school_id == user.school_id
        end
        can :show, User, id: user.id
      elsif user.type == 'StudentUser'
        can %i[index show], [Characteristic, City, PersonalValueGroup, Role, School]
        can [:show, :index], StudentGroup do |g|
          g.id == user.student.student_group_id
        end
        can [:show, :index], Student do |s|
          s.id == user.student_id
        end
        can :manage, StudentTest do |st|
          st.student_id == user.student_id && st.test &&
            st.test.student_group_id == user.student.student_group_id
        end
        can :manage, Person do |p|
          user.student.student_test_ids.include?(p.student_test_id)
        end
        can :manage, PersonRole do |pr|
          pr.person && user.student.student_test_ids.include?(pr.person.student_test_id)
        end
        can :manage, PersonCharacteristic do |pc|
          pc.person && user.student.student_test_ids.include?(pc.person.student_test_id)
        end
        can [:show, :read], Test do |t|
          t.student_group_id == user.student.student_group_id
        end
        can :show, User, id: user.id
      elsif user.type == 'SchoolPsychologist'
        can %i[show index], [Characteristic, City, PersonalValueGroup, Role, School]
        can %i[show index], Person do |p|
          p.student_test && p.student_test.test.student_group.school_id == user.school_id
        end
        can %i[show index], PersonRole do |pr|
          pr.person && pr.person.student_test.test.student_group.school_id == user.school_id
        end
        can %i[show index], PersonCharacteristic do |pc|
          pc.person && pc.person.student_test.test.student_group.school_id == user.school_id
        end
        can %i[show index], StudentGroup, school_id: user.school_id
        can %i[show index], Student do |s|
          s.student_group && s.student_group.school_id == user.school_id
        end
        can :manage, Test do |t|
          t.student_group && t.student_group.school_id == user.school_id
        end
        can %i[show index], StudentTest do |st|
          st.test && st.test.student_group.school_id == user.school_id
        end
        can :result, Test do |t|
          t.student_group && t.student_group.school_id == user.school_id
        end
        can :show, User, id: user.id
      end
    end
  end
end
