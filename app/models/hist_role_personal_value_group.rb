class HistRolePersonalValueGroup < ApplicationRecord
  belongs_to :role
  belongs_to :personal_value_group
  belongs_to :test_result
  
  enum direction: %i[role_row group_row]

  validates :value, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 1}
end
