class PersonCharacteristic < ApplicationRecord
  belongs_to :person
  belongs_to :characteristic

  validates_uniqueness_of :characteristic_id, scope: :person

  filterrific :available_filters => [
      :with_person_id,
      :with_characteristic_id
  ]

  scope :with_person_id, ->(id){
    where(:person_id => [*id])
  }

  scope :with_characteristic_id, ->(id){
    where(:characteristic_id => [*id])
  }

end
