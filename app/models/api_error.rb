class ApiError
  include ActiveModel::Serialization
  attr_reader :errors

  private

  def initialize(errors)
    @errors = errors
  end

  public

  def self.can_not_save
    ApiErrors.new ['При сохранении возникли ошибки. Обратитесь к администратору.']
  end

  def self.not_found
    ApiError.new ['Ресурс не найден']
  end

  def self.forbidden
    ApiError.new ['Доступ запрещен']
  end

  def self.login
    ApiError.new ['Неверный логин или пароль']
  end

  def self.get_not_supported_auth
    ApiError.new ['Используйте POST /sign_in для входа. GET запросы не поддерживаются.']
  end

  def self.invalid_routing
    ApiError.new ['Неверный url']
  end

  def self.bad_request
    ApiError.new ['Неверный формат параметров.']
  end

  def self.invalid_student_token
    ApiError.new ['Неверный токен']
  end

  def self.student_user_already_exist
    ApiError.new ['Вы уже регистрировались.']
  end

  def self.internal_error
    ApiError.new ['Внутренняя ошибка сервера. Обратитесь к администратору.']
  end
end

