class AuthorityRole < ApplicationRecord
  belongs_to :voluation_profile
  belongs_to :role
  belongs_to :test_result

  validates :value, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 1}
end
