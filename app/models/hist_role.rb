class HistRole < ApplicationRecord
  belongs_to :role
  belongs_to :test_result

  validates :generally, :indicate_procent, :not_indicate_procent, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 1}
  validates :indicate, :not_indicate, numericality: {only_integer: true, greater_than_or_equal_to: 0}
end
