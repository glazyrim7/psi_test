class StudentGroup < ApplicationRecord
  belongs_to :school
  has_many :students

  validates :letter, :form_date, presence: true
  validates_uniqueness_of :letter, scope: [:form_date, :school_id]

  filterrific :available_filters => [
      :with_school_id
  ]

  scope :with_school_id, ->(id){
    where(:school_id => [*id])
  }

  def is_active
    return(true) if form_date + 11 > Date.current
    false
  end

  def name
    current_course_number.to_s + letter
  end

  private

  def current_course_number
    ((Date.current - form_date)/365).to_i + 1 
  end

end
