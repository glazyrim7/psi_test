class PersonalValueGroup < ApplicationRecord
  validates :name, :human_name, presence: true
  validates :name, uniqueness: {case_sensitive: true}
  validates :human_name, uniqueness: {case_sensitive: true}

  has_many :first_groups, class_name: "AntonymPersonValueGroup", foreign_key: "first_group_id"
  has_many :second_groups, class_name: "AntonymPersonValueGroup", foreign_key: "second_group_id"

  filterrific :available_filters => [
      :with_name,
      :with_human_name
  ]

  scope :with_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(name) LIKE ?", "%#{query}%".downcase)
  }

  scope :with_human_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(human_name) LIKE ?", "%#{query}%".downcase)
  }

end
