class PairRole < ApplicationRecord
  belongs_to :first_role, class_name: "Role"
  belongs_to :second_role, class_name: "Role"
  belongs_to :test_result
  
  validates :eq_percent, :gt_percent, :lt_percent, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 1}
  validates :eq_count, :gt_count, :lt_count, numericality: {only_integer: true, greater_than_or_equal_to: 0}

end
