class City < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: {case_sensitive: true}

  filterrific :available_filters => [
      :with_name
  ]

  scope :with_name, lambda { |query|
    return nil if query.blank?
    where("LOWER(name) LIKE ?", "%#{query}%".downcase)
  }
end
