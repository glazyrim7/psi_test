class SocPortrait < ApplicationRecord
  belongs_to :test_result
  belongs_to :voluation_profile
  validates :roles, :characteristics, :groups, presence: true
end
