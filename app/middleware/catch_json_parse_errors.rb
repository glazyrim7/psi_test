class CatchJsonParseErrors
  def initialize(app)
    @app = app
  end

  def call(env)
    begin
      @app.call(env)
    rescue ActionDispatch::Http::Parameters::ParseError => error
      return [
          400, { "Content-Type" => "application/json" },
          [ApiError.bad_request.to_json]
        ]
    end
  end
end
