class CharacteristicSerializer < ActiveModel::Serializer
  attributes :id, :name, :positivity, :personal_value_group_id
end
