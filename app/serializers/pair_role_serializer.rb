class PairRoleSerializer < ActiveModel::Serializer
  attributes :first_role, :second_role, :eq_count, :eq_percent, :lt_count, :lt_percent, :gt_count, :gt_percent

  def first_role
    object.first_role.name
  end

  def second_role
    object.second_role.name
  end
end
