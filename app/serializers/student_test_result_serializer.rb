class StudentTestResultSerializer < ActiveModel::Serializer
  attributes :id
  belongs_to :first_part_result, serializer: FirstPartResultSerializer
  belongs_to :second_part_result, serializer: SecondPartResultSerializer
  belongs_to :third_part_result, serializer: ThirdPartResultSerializer
end
