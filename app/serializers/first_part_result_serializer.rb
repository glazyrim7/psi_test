class FirstPartResultSerializer < ActiveModel::Serializer
  attributes :older_people, :peers, :male, :female, :relatives, :study_place, :real_comm, :inet_com, :fict_chars,
             :famous, :friends, :study, :sport, :science, :politics, :army, :art, :max_categories, :act_areas,
             :empty, :business
end
