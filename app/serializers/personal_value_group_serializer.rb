class PersonalValueGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :human_name
end
