class TestResultSerializer < ActiveModel::Serializer
  attributes :id, :hist_roles_groups, :hist_group_roles
  has_many :hist_roles
  has_many :hist_personal_value_groups
  has_many :pair_roles
  has_many :authority_roles
  has_many :soc_portraits
  has_many :corr_terminal_instrumental_values
  has_many :hist_self_esteems

  def hist_roles_groups
    ActiveModel::SerializableResource.new(self.object.hist_role_personal_value_groups.select { |item| item.direction == 'role_row' },
                                          each_serializer: HistRolePersonalValueGroupSerializer).as_json
  end

  def hist_group_roles
    ActiveModel::SerializableResource.new(self.object.hist_role_personal_value_groups.select { |item| item.direction == 'group_row' },
                                          each_serializer: HistRolePersonalValueGroupSerializer).as_json
  end

end
