class HistSelfEsteemSerializer < ActiveModel::Serializer
  attributes :self_esteem_type_name, :percent, :value

  def self_esteem_type_name
    object.self_esteem_type.name
  end
end
