class PersonRoleSerializer < ActiveModel::Serializer
  attributes :id, :person_id, :role_id
end
