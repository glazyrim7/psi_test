class CorrTerminalInstrumentalValueSerializer < ActiveModel::Serializer
  attributes :valuation_profile_name, :respondents_number, :without_respondents_number, :frequency

  def valuation_profile_name
    object.voluation_profile.name
  end
end
