class PersonFullSerializer < APersonSerializer
  has_many :person_roles, :key => "person_roles_attributes",
           each_serializer: NestedPersonRoleSerializer
  has_many :person_characteristics, :key => "person_characteristics_attributes",
           each_serializer: NestedPersonCharsSerializer
end
