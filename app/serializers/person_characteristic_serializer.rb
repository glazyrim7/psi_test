class PersonCharacteristicSerializer < ActiveModel::Serializer
  attributes :id, :person_id, :characteristic_id
end
