class StudentGroupSerializer < ActiveModel::Serializer
  attributes :id, :school_id, :form_date, :letter, :name
end
