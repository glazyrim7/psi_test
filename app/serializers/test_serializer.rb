class TestSerializer < ActiveModel::Serializer
  attributes :id, :student_group_id, :start, :finish
end
