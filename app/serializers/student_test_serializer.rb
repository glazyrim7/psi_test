class StudentTestSerializer < ActiveModel::Serializer
  attributes :id, :test_id, :student_id, :student_test_result_id
end
