class AuthorityRoleSerializer < ActiveModel::Serializer
  attributes :value, :role_name, :valuation_profile_name

  def role_name
    object.role.name
  end

  def valuation_profile_name
    object.voluation_profile.name
  end
end
