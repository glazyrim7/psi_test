class SocPortraitSerializer < ActiveModel::Serializer
  attributes :roles, :characteristics, :groups, :valuation_profile_name

  def valuation_profile_name
    object.voluation_profile.name
  end
end
