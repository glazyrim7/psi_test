class StudentTestFullSerializer < StudentTestSerializer
  has_many :persons, :key => "persons_attributes", serializer: NestedPersonSerializer
end
