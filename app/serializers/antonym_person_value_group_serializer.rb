class AntonymPersonValueGroupSerializer < ActiveModel::Serializer
  attributes :id, :first_group_id, :second_group_id
end
