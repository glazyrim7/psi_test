#!/bin/bash

rm -f psi/tmp/pids/server.pid

bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
bundle exec rails server -b 0.0.0.0 
