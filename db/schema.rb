# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_01_151707) do

  create_table "antonym_person_value_groups", force: :cascade do |t|
    t.integer "first_group_id"
    t.integer "second_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["first_group_id"], name: "index_antonym_person_value_groups_on_first_group_id"
    t.index ["second_group_id"], name: "index_antonym_person_value_groups_on_second_group_id"
  end

  create_table "antonyms", force: :cascade do |t|
    t.integer "first_char_id"
    t.integer "second_char_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["first_char_id"], name: "index_antonyms_on_first_char_id"
    t.index ["second_char_id"], name: "index_antonyms_on_second_char_id"
  end

  create_table "authority_roles", force: :cascade do |t|
    t.integer "voluation_profile_id"
    t.integer "role_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_result_id"
    t.index ["role_id"], name: "index_authority_roles_on_role_id"
    t.index ["test_result_id"], name: "index_authority_roles_on_test_result_id"
    t.index ["voluation_profile_id"], name: "index_authority_roles_on_voluation_profile_id"
  end

  create_table "characteristics", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "personal_value_group_id"
    t.float "positivity"
    t.index ["personal_value_group_id"], name: "index_characteristics_on_personal_value_group_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "corr_terminal_instrumental_values", force: :cascade do |t|
    t.integer "test_result_id"
    t.integer "voluation_profile_id"
    t.float "frequency"
    t.integer "respondents_number"
    t.integer "without_respondents_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["test_result_id"], name: "index_corr_terminal_instrumental_values_on_test_result_id"
    t.index ["voluation_profile_id"], name: "index_ctiv_voluation_profile_id"
  end

  create_table "first_part_results", force: :cascade do |t|
    t.integer "older_people"
    t.integer "peers"
    t.integer "male"
    t.integer "female"
    t.integer "relatives"
    t.integer "study_place"
    t.integer "real_comm"
    t.integer "inet_com"
    t.integer "fict_chars"
    t.integer "famous"
    t.integer "friends"
    t.integer "study"
    t.integer "sport"
    t.integer "science"
    t.integer "politics"
    t.integer "army"
    t.integer "art"
    t.string "max_categories"
    t.string "act_areas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "empty"
    t.integer "business"
  end

  create_table "hist_personal_value_groups", force: :cascade do |t|
    t.integer "personal_value_group_id"
    t.float "value"
    t.integer "indicate"
    t.float "indicate_percent"
    t.float "not_indicate_percent"
    t.integer "not_indicate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_result_id"
    t.index ["personal_value_group_id"], name: "index_hist_personal_value_groups_on_personal_value_group_id"
    t.index ["test_result_id"], name: "index_hist_personal_value_groups_on_test_result_id"
  end

  create_table "hist_role_personal_value_groups", force: :cascade do |t|
    t.integer "role_id"
    t.integer "personal_value_group_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "direction"
    t.integer "test_result_id"
    t.index ["personal_value_group_id"], name: "index_hist_role_personal_value_group_id"
    t.index ["role_id"], name: "index_hist_role_personal_value_groups_on_role_id"
    t.index ["test_result_id"], name: "index_hist_role_personal_value_groups_on_test_result_id"
  end

  create_table "hist_roles", force: :cascade do |t|
    t.integer "role_id"
    t.float "generally"
    t.integer "indicate"
    t.float "indicate_procent"
    t.integer "not_indicate"
    t.float "not_indicate_procent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_result_id"
    t.index ["role_id"], name: "index_hist_roles_on_role_id"
    t.index ["test_result_id"], name: "index_hist_roles_on_test_result_id"
  end

  create_table "hist_self_esteems", force: :cascade do |t|
    t.integer "test_result_id"
    t.integer "self_esteem_type_id"
    t.integer "value"
    t.float "percent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["self_esteem_type_id"], name: "index_hist_self_esteems_on_self_esteem_type_id"
    t.index ["test_result_id"], name: "index_hist_self_esteems_on_test_result_id"
  end

  create_table "pair_roles", force: :cascade do |t|
    t.integer "first_role_id"
    t.integer "second_role_id"
    t.integer "eq_count"
    t.float "eq_percent"
    t.integer "gt_count"
    t.float "gt_percent"
    t.integer "lt_count"
    t.float "lt_percent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_result_id"
    t.index ["first_role_id"], name: "index_pair_roles_on_first_role_id"
    t.index ["second_role_id"], name: "index_pair_roles_on_second_role_id"
    t.index ["test_result_id"], name: "index_pair_roles_on_test_result_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.float "x"
    t.float "y"
    t.integer "student_test_id"
    t.integer "characteristic_id"
    t.integer "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["characteristic_id"], name: "index_people_on_characteristic_id"
    t.index ["role_id"], name: "index_people_on_role_id"
    t.index ["student_test_id"], name: "index_people_on_student_test_id"
  end

  create_table "person_characteristics", force: :cascade do |t|
    t.integer "person_id"
    t.integer "characteristic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["characteristic_id"], name: "index_person_characteristics_on_characteristic_id"
    t.index ["person_id"], name: "index_person_characteristics_on_person_id"
  end

  create_table "person_roles", force: :cascade do |t|
    t.integer "person_id"
    t.integer "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_person_roles_on_person_id"
    t.index ["role_id"], name: "index_person_roles_on_role_id"
  end

  create_table "personal_value_groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "human_name"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.index ["position"], name: "index_roles_on_position", unique: true
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_schools_on_city_id"
  end

  create_table "second_part_results", force: :cascade do |t|
    t.string "leading_values"
    t.string "rejected"
    t.string "personal_resources"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "self_esteem_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "soc_portraits", force: :cascade do |t|
    t.integer "test_result_id"
    t.integer "voluation_profile_id"
    t.string "roles"
    t.string "characteristics"
    t.string "groups"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["test_result_id"], name: "index_soc_portraits_on_test_result_id"
    t.index ["voluation_profile_id"], name: "index_soc_portraits_on_voluation_profile_id"
  end

  create_table "student_groups", force: :cascade do |t|
    t.integer "school_id"
    t.string "letter"
    t.date "form_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_id"], name: "index_student_groups_on_school_id"
  end

  create_table "student_test_results", force: :cascade do |t|
    t.integer "first_part_result_id"
    t.integer "second_part_result_id"
    t.integer "third_part_result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["first_part_result_id"], name: "index_student_test_results_on_first_part_result_id"
    t.index ["second_part_result_id"], name: "index_student_test_results_on_second_part_result_id"
    t.index ["third_part_result_id"], name: "index_student_test_results_on_third_part_result_id"
  end

  create_table "student_tests", force: :cascade do |t|
    t.integer "student_id"
    t.integer "test_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "student_test_result_id"
    t.index ["student_id"], name: "index_student_tests_on_student_id"
    t.index ["student_test_result_id"], name: "index_student_tests_on_student_test_result_id"
    t.index ["test_id"], name: "index_student_tests_on_test_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "student_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sex"
    t.string "token"
    t.index ["student_group_id"], name: "index_students_on_student_group_id"
  end

  create_table "test_results", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tests", force: :cascade do |t|
    t.integer "student_group_id"
    t.datetime "start"
    t.datetime "finish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_result_id"
    t.index ["student_group_id"], name: "index_tests_on_student_group_id"
    t.index ["test_result_id"], name: "index_tests_on_test_result_id"
  end

  create_table "third_part_results", force: :cascade do |t|
    t.string "optimist"
    t.string "self_esteem"
    t.string "equivalent"
    t.string "ideals"
    t.string "antiideals"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "login", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "login"
    t.text "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.integer "student_id"
    t.integer "school_id"
    t.index ["login"], name: "index_users_on_login", unique: true
    t.index ["school_id"], name: "index_users_on_school_id"
    t.index ["student_id"], name: "index_users_on_student_id"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "voluation_profiles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
