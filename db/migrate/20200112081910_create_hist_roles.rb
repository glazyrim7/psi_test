class CreateHistRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_roles do |t|
      t.references :role, foreign_key: true
      t.references :test, foreign_key: true
      t.float :generally
      t.integer :indicate
      t.float :indicate_procent
      t.integer :not_indicate
      t.float :not_indicate_procent

      t.timestamps
    end
  end
end
