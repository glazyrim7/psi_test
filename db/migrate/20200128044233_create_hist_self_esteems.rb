class CreateHistSelfEsteems < ActiveRecord::Migration[5.2]
  def change
    create_table :hist_self_esteems do |t|
      t.references :test_result, foreign_key: true
      t.references :self_esteem_type, foreign_key: true
      t.integer :value
      t.float :percent

      t.timestamps
    end
  end
end
