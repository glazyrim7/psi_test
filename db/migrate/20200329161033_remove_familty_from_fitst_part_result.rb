class RemoveFamiltyFromFitstPartResult < ActiveRecord::Migration[5.2]
  def change
    remove_column :first_part_results, :family
  end
end
