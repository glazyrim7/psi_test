class CreateAuthorityRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :authority_roles do |t|
      t.references :voluation_profile, foreign_key: true
      t.references :role, foreign_key: true
      t.float :value

      t.timestamps
    end
  end
end
