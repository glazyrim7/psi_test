class CreateSocPortraits < ActiveRecord::Migration[5.2]
  def change
    create_table :soc_portraits do |t|
      t.references :test_result, foreign_key: true
      t.references :voluation_profile, foreign_key: true
      t.string :roles
      t.string :characteristics
      t.string :groups

      t.timestamps
    end
  end
end
