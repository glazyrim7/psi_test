class ChangeTestToTestResultPairRole < ActiveRecord::Migration[5.2]
  def change
    remove_reference :pair_roles, :test, foreign_key: true
    add_reference :pair_roles, :test_result, foreign_key: true
  end
end
