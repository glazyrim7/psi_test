class CreateFirstPartResults < ActiveRecord::Migration[5.2]
  def change
    create_table :first_part_results do |t|
      t.integer :older_people
      t.integer :peers
      t.integer :male
      t.integer :female
      t.integer :relatives
      t.integer :study_place
      t.integer :real_comm
      t.integer :inet_com
      t.integer :fict_chars
      t.integer :famous
      t.integer :family
      t.integer :friends
      t.integer :study
      t.integer :sport
      t.integer :science
      t.integer :politics
      t.integer :army
      t.integer :art
      t.string :max_categories
      t.string :act_areas

      t.timestamps
    end
  end
end
