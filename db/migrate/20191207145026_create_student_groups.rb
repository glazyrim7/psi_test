class CreateStudentGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :student_groups do |t|
      t.references :school, foreign_key: true
      t.string :letter
      t.date :form_date

      t.timestamps
    end
  end
end
