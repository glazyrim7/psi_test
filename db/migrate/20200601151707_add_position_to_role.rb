class AddPositionToRole < ActiveRecord::Migration[5.2]
  def change
    add_column :roles, :position, :integer
    add_index :roles, :position, unique: true
  end
end
