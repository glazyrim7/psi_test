class CreatePersonalValueGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_value_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
