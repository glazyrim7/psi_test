class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.float :x
      t.float :y
      t.references :student_test, foreign_key: true
      t.references :characteristic, foreign_key: true
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
