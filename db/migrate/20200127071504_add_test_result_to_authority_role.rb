class AddTestResultToAuthorityRole < ActiveRecord::Migration[5.2]
  def change
    add_reference :authority_roles, :test_result, foreign_key: true
  end
end
