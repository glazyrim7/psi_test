class ChangeTestToTestResultInHistPersonalValueGroup < ActiveRecord::Migration[5.2]
  def change
    remove_reference :hist_role_personal_value_groups, :test, foreign_key: true
    add_reference :hist_role_personal_value_groups, :test_result, foreign_key: true
  end
end
