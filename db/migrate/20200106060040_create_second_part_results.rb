class CreateSecondPartResults < ActiveRecord::Migration[5.2]
  def change
    create_table :second_part_results do |t|
      t.string :leading_values
      t.string :rejected
      t.string :personal_resources

      t.timestamps
    end
  end
end
