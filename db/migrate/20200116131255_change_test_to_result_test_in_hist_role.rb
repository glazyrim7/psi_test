class ChangeTestToResultTestInHistRole < ActiveRecord::Migration[5.2]
  def change
    remove_reference :hist_roles, :test, foreign_key: true
    add_reference :hist_roles, :test_result, foreign_key: true
  end
end