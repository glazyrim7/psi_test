class AddInheritanceToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :school, index: true
    add_column :users, :type, :string
  end
end
