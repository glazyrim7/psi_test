class RemoveSchoolFromPsyhologist < ActiveRecord::Migration[5.2]
  def change
    remove_reference :users, :school
  end
end
