class CreateAntonyms < ActiveRecord::Migration[5.2]
  def change
    create_table :antonyms do |t|
      t.references :first_char, references: :characteristics
      t.references :second_char, references: :characteristics
      t.timestamps
    end
  end
end
