class AddResultIdToStudentTest < ActiveRecord::Migration[5.2]
  def change
    add_reference :student_tests, :student_test_result, index:true
  end
end
