class AddPositiveToCharacteristic < ActiveRecord::Migration[5.2]
  def change
    add_column :characteristics, :positivity, :float
  end
end
