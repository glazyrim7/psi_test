class CreateStudentTestResults < ActiveRecord::Migration[5.2]
  def change
    create_table :student_test_results do |t|
      t.references :first_part_result, foreign_key: true
      t.references :second_part_result, foreign_key: true
      t.references :third_part_result, foreign_key: true

      t.timestamps
    end
  end
end
