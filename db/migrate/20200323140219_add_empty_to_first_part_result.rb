class AddEmptyToFirstPartResult < ActiveRecord::Migration[5.2]
  def change
    add_column :first_part_results, :empty, :string
  end
end
