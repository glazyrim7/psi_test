class AddTestTestResult < ActiveRecord::Migration[5.2]
  def change
    add_reference :tests, :test_result, foreign_key: true
  end
end
