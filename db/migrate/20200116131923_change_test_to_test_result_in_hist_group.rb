class ChangeTestToTestResultInHistGroup < ActiveRecord::Migration[5.2]
  def change
    remove_reference :hist_personal_value_groups, :test, foreign_key: true
    add_reference :hist_personal_value_groups, :test_result, foreign_key: true
  end
end
