class CreateSelfEsteemTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :self_esteem_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
