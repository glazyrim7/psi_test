class CreateTests < ActiveRecord::Migration[5.2]
  def change
    create_table :tests do |t|
      t.references :student_group, foreign_key: true
      t.datetime :start
      t.datetime :finish

      t.timestamps
    end
  end
end
