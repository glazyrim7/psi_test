Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      sessions:  'overrides/sessions',
      token_validations:  'overrides/token_validations'
  }
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  resources :roles, only: [:show, :index]
  resources :characteristics, only: [:show, :index, :create, :update, :destroy]
  resources :cities, only: [:show, :index, :create, :update, :destroy]
  resources :schools, only: [:show, :index, :create, :update, :destroy]
  resources :personal_value_groups, only: [:show, :index]
  resources :student_groups, only: [:show, :index, :create, :update, :destroy]
  resources :students, only: [:show, :index, :create, :update, :destroy]
  resources :tests, only: [:show, :index, :create, :update, :destroy] do
    member do
      get 'result'
    end
  end
  resources :student_tests, only: [:show, :index, :create, :update, :destroy] do
    member do
      get 'result'
    end
  end
  resources :persons, only: [:show, :index, :create, :update, :destroy]
  resources :person_roles, only: [:show, :index, :create, :update, :destroy]
  resources :person_characteristics, only: [:show, :index, :create, :update, :destroy]
#  resources :antonyms, only: [:show, :index, :create, :update, :destroy]
#  resources :antonym_person_value_groups, only: [:show, :index, :create, :update, :destroy]
  resources :users, only: [:show, :index, :create, :update, :destroy]

  post "/sign_up" => "registration#sign_up", :as => :sign_up
  match '*path', via: :all, :to => 'application#record_not_found'
end
